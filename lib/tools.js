const inquirer = require('inquirer')
const shell = require('shelljs');
const hasYarn = shell.exec('yarn -v', { silent: true }).code === 0

exports.selectPkgManagement = () => {
  if (!hasYarn) return Promise.resolve('npm')
  return inquirer.prompt([{
    type: 'list',
    name: 'type',
    message: '选择使用的包管理器：',
    default: 'yarn',
    choices: ['yarn', 'npm']
  }]).then(m => m.type)
}

exports.inputBranch = () => {
  return inquirer.prompt([{
    type: 'input',
    name: 'branch',
    message: '输入需要切换的分支(为空则不改变分支)：',
  }]).then(m => m.branch)
}

exports.autoPush = () => {
  return inquirer.prompt([{
    type: 'confirm',
    name: 'autoPush',
    message: '是否自动推送(push)：',
  }]).then(m => m.autoPush)
}
