## multiup
一个在多个仓库中升级包版本的工具

### 使用方式

安装：

```shell
npm i multi-update-cli
```

命令： multiup update \<packageName> \<version> [dirs...]

- packageName: 需要升级包的包名
- version: 需升级到的版本
- dirs: 需升级仓库目录名，不指定选项时等同于选项 --some， 当指定选项为 --all 时可不设置该参数。

Options:  
    -a, --all   升级当前目录下所有仓库  
    -s, --some  升级指定仓库<默认>  
    -h, --help  帮助  

#### 示例

```shell
# 升级指定仓库(app-1, app-2, app-3)
multiup update shelljs 0.8.4 app-1 app-2 app-3

# 升级当前目录下所有仓库
multiup update -a shelljs 0.8.4
